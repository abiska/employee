package employee;


public class PayrollSimulation {


    public static void main(String[] args) {
        
        Employee emp = new Employee("Tom", 10, 20);
        Employee mng = new Manager("Jerry", 50, 60, 70);
        
        
            
        System.out.println( emp.getName() + "'s paycheck = $" + emp.calculatePay());
    
        System.out.println("\n");
        
        System.out.println( mng.getName() + "'s paycheck = $" + mng.calculatePay() );
    }

}
